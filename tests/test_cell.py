from unittest import TestCase
import env
import pymaze


class TestCell(TestCase):
    def test_eq(self):
        grid = pymaze.Grid(2, 2)
        self.assertEqual(grid[1, 0], grid[1, 0])
        self.assertEqual(grid[0, 1], grid[0, 1])
        self.assertNotEqual(grid[0, 1], grid[1, 0])

    def test_init(self):
        grid = pymaze.Grid(2, 3)
        cell1 = pymaze.Cell(grid, 0, 0)
        self.assertEqual(cell1, grid[0, 0])
        cell2 = pymaze.Cell(grid, 1, 2)
        self.assertEqual(cell2, grid[1, 2])

    def test_neighbours(self):
        grid = pymaze.Grid(3, 3)
        a = grid[1, 1]
        self.assertEqual(a.north, grid[0, 1])
        self.assertEqual(a.south, grid[2, 1])
        self.assertEqual(a.east, grid[1, 2])
        self.assertEqual(a.west, grid[1, 0])

        self.assertEqual(len(grid[0, 0].neighbours()), 2)
        self.assertEqual(len(grid[0, 1].neighbours()), 3)
        self.assertEqual(len(grid[0, 2].neighbours()), 2)
        self.assertEqual(len(grid[1, 0].neighbours()), 3)
        self.assertEqual(len(grid[1, 1].neighbours()), 4)
        self.assertEqual(len(grid[1, 2].neighbours()), 3)
        self.assertEqual(len(grid[2, 0].neighbours()), 2)
        self.assertEqual(len(grid[2, 1].neighbours()), 3)
        self.assertEqual(len(grid[2, 2].neighbours()), 2)

    def test_linked(self):
        grid = pymaze.Grid(2, 2)
        a = grid[0, 0]
        b = grid[0, 1]
        self.assertEqual(len(a.links()), 0)
        self.assertEqual(len(b.links()), 0)
        a.link(b)
        self.assertTrue(a.is_linked(b))
        self.assertTrue(b.is_linked(a))
        self.assertEqual(len(a.links()), 1)
        self.assertEqual(len(b.links()), 1)
        b.unlink(a)
        self.assertEqual(len(a.links()), 0)
        self.assertEqual(len(b.links()), 0)
        b.link(a)
        self.assertTrue(a.is_linked(b))
        self.assertTrue(b.is_linked(a))

    def test_links(self):
        grid = pymaze.Grid(2, 2)
        a = grid[0, 0]
        b = grid[0, 1]
        c = grid[1, 1]
        a.link(b)
        b.link(c)
        a_links = a.links()
        self.assertEqual(len(a_links), 1)
        self.assertEqual(a_links[0], b)
        b_links = b.links()
        self.assertEqual(len(b_links), 2)
        self.assertEqual(b_links[0], c)
        self.assertEqual(b_links[1], a)
        c_links = c.links()
        self.assertEqual(len(c_links), 1)
        self.assertEqual(c_links[0], b)
