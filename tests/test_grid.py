from unittest import TestCase
import env
import pymaze


class TestGrid(TestCase):
    def test_init(self):
        grid = pymaze.Grid(2, 3)
        self.assertEqual(grid.rows, 2)
        self.assertEqual(grid.columns, 3)
        self.assertEqual(grid.size, 6)

        # Grids must be at least 2x2
        self.assertRaises(pymaze.GridException, pymaze.Grid, 1, 3)
        self.assertRaises(pymaze.GridException, pymaze.Grid, 3, 1)

    def test_cells(self):
        grid = pymaze.Grid(2, 2)
        cell1 = grid[0, 0]
        self.assertEqual(cell1.row, 0)
        self.assertEqual(cell1.column, 0)
        cell2 = grid[1, 1]
        self.assertEqual(cell2.row, 1)
        self.assertEqual(cell2.column, 1)
        cell3 = grid[2, 2]
        self.assertEqual(cell3, None)

    def test_links(self):
        grid = pymaze.Grid(3, 3)
        a = grid[1, 1]
        b = grid[1, 2]
        c = grid[2, 2]
        # a -> b is horizontal link, b -> c is vertical link
        self.assertFalse(grid.get_link(a, b))
        self.assertFalse(grid.get_link(b, c))
        grid.set_link(a, b, True)
        self.assertTrue(grid.get_link(a, b))
        self.assertFalse(grid.get_link(b, c))
        grid.set_link(b, c, True)
        self.assertTrue(grid.get_link(a, b))
        self.assertTrue(grid.get_link(b, c))
        grid.set_link(a, b, False)
        self.assertFalse(grid.get_link(a, b))
        self.assertTrue(grid.get_link(b, c))
        grid.set_link(b, c, False)
        self.assertFalse(grid.get_link(a, b))
        self.assertFalse(grid.get_link(b, c))

    def test_random(self):
        grid = pymaze.Grid(3, 5)
        cells = [grid.random_cell() for _ in range(10)]
        self.assertTrue(all(cells))
