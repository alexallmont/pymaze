import unittest
import glob
import pep8


class TestCodeFormat(unittest.TestCase):
    def test_pep8_conformance(self):
        input_filenames = glob.glob('pymaze/*.py') + glob.glob('tests/*.py')

        pep8_style = pep8.StyleGuide()
        result = pep8_style.check_files(input_filenames)
        self.assertEqual(
            result.total_errors, 0,
            "Found code style errors (and warnings)."
        )
