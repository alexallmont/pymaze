import numpy as np
import random
from .cell import Cell


class GridException(Exception):
    pass


def _relative_bitmask(cell1, cell2):
    if cell2 == cell1.north:
        return 1
    elif cell2 == cell1.south:
        return 2
    elif cell2 == cell1.east:
        return 4
    elif cell2 == cell1.west:
        return 8

    raise GridException(
        "Cell {} is not a neighbour of {}".format(cell2, cell1)
    )


class Grid:
    def __init__(self, rows, columns):
        if rows < 2 or columns < 2:
            raise GridException("Grid size must be at least 2 by 2")

        self.cell_links = np.zeros([rows, columns], dtype=int)

    @property
    def rows(self):
        return self.cell_links.shape[0]

    @property
    def columns(self):
        return self.cell_links.shape[1]

    @property
    def size(self):
        return self.rows * self.columns

    def __getitem__(self, rc_tuple):
        row, column = rc_tuple
        if 0 <= row < self.rows:
            if 0 <= column < self.columns:
                return Cell(self, row, column)
        return None

    def random_cell(self):
        index = random.randint(0, self.size - 1)
        column, row = divmod(index, self.rows)
        return self[row, column]

    def set_link(self, cell1, cell2, linked):
        bitmask1 = _relative_bitmask(cell1, cell2)
        bitmask2 = _relative_bitmask(cell2, cell1)
        if linked:
            self.cell_links[cell1.row, cell1.column] |= bitmask1
            self.cell_links[cell2.row, cell2.column] |= bitmask2
        else:
            self.cell_links[cell1.row, cell1.column] &= 15 - bitmask1
            self.cell_links[cell2.row, cell2.column] &= 15 - bitmask2

    def get_link(self, cell1, cell2):
        if not cell1 or not cell2:
            return False

        bitmask = _relative_bitmask(cell1, cell2)
        bit_set = self.cell_links[cell1.row, cell1.column] & bitmask
        return bit_set != 0
