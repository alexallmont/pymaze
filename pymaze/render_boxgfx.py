# 2^4 celles index by bit 0 = North, 1 = South, 2 = West, 3 = East.
# A bit being set indicates cell opens on that edge.
# First line is cell height.
_boxgfx = """3
┏━━━┓
┃   ┃
┗━━━┛
┃   ┃
┃   ┃
┗━━━┛
┏━━━┓
┃   ┃
┃   ┃
┃   ┃
┃   ┃
┃   ┃
┏━━━━
┃
┗━━━━
┃   ┗
┃
┗━━━━
┏━━━━
┃
┃   ┏
┃   ┗
┃
┃   ┏
━━━━┓
    ┃
━━━━┛
┛   ┃
    ┃
━━━━┛
━━━━┓
    ┃
┓   ┃
┛   ┃
    ┃
┓   ┃
━━━━━

━━━━━
┛   ┗

━━━━━
━━━━━

┓   ┏
┛   ┗

┓   ┏
"""


def _init_boxgfx_chars():
    """Load characters from string definition"""
    lines = _boxgfx.split('\n')

    # First line read is number of rows or height of each cell
    height = int(lines[0])

    # Read in 16 * height lines for all NWEW compass combinations
    for i in range(16):
        char = []
        for row in range(height):
            index = 1 + row + i * height
            char.append(lines[index].ljust(5))
        _boxgfx_chars.append(char)


# Initialize box characters on import
_boxgfx_chars = []
_init_boxgfx_chars()


def render_boxgfx(grid):
    """Render grid using customisable gfx characters"""
    for row in range(grid.rows):
        for sub_row in range(3):
            line = ''
            for column in range(grid.columns):
                cell = grid[row, column]
                index = 0
                index += 1 if cell.is_linked(cell.north) else 0
                index += 2 if cell.is_linked(cell.south) else 0
                index += 4 if cell.is_linked(cell.east) else 0
                index += 8 if cell.is_linked(cell.west) else 0
                line += _boxgfx_chars[index][sub_row]
            print(line)
