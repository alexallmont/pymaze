def _render(grid, hwall, vwall, junc):
    hspace = ' ' * len(hwall)
    vspace = ' ' * len(vwall)
    vert_walls = [
        [hspace if link else hwall for link in grid.vert_links[row]]
        for row in range(grid.rows - 1)
    ]
    horz_walls = [
        [vspace if link else vwall for link in grid.horz_links[row]]
        for row in range(grid.rows)
    ]
    horz_ends = [junc] * (grid.columns + 1)

    print(hwall.join(horz_ends))
    for row in range(grid.rows - 1):
        print(hspace.join([vwall] + horz_walls[row] + [vwall]))
        print(junc + junc.join(vert_walls[row]) + junc)
    print(hspace.join([vwall] + horz_walls[-1] + [vwall]))
    print(hwall.join(horz_ends))


def render_ascii(grid):
    """
    Draw a maze grid using standard ASCII characters -, | and +
    """
    _render(grid, '---', '|', '+')


def render_boxchar(grid):
    """
    Draw a maze grid using Unicode box characters
    """
    # https://en.wikipedia.org/wiki/Box-drawing_character
    _render(grid, u'\u2501' * 3, u'\u2503', u'\u254b')


def render_blockchar(grid):
    """
    Draw a maze grid using Unicode block characters
    """
    blk = u'\u2593'
    _render(grid, blk * 2, blk, blk)
