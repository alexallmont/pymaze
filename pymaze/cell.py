class Cell:
    def __init__(self, grid, row, column):
        self.grid = grid
        self.row = row
        self.column = column

    def __eq__(self, other):
        return (
            other and
            self.grid == other.grid and
            self.row == other.row and
            self.column == other.column
        )

    def link(self, cell):
        self.grid.set_link(self, cell, True)

    def unlink(self, cell):
        self.grid.set_link(self, cell, False)

    def links(self):
        return [n for n in self.neighbours() if self.is_linked(n)]

    def is_linked(self, cell):
        return self.grid.get_link(self, cell)

    @property
    def north(self):
        return self.grid[self.row - 1, self.column]

    @property
    def south(self):
        return self.grid[self.row + 1, self.column]

    @property
    def east(self):
        return self.grid[self.row, self.column + 1]

    @property
    def west(self):
        return self.grid[self.row, self.column - 1]

    def neighbours(self):
        return list(filter(
            lambda n: n is not None,
            [self.north, self.south, self.east, self.west]
        ))
