import random


def generate_aldous_broder(grid):
    cell = grid.random_cell()
    unvisited = grid.size - 1

    while unvisited > 0:
        neighbour = random.choice(cell.neighbours())

        if not neighbour.links():
            cell.link(neighbour)
            unvisited -= 1

        cell = neighbour
