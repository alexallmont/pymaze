import sys


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('Supply number of rows and columns as arguments')
        exit()

    from os import path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    import pymaze

    rows = int(sys.argv[1])
    columns = int(sys.argv[2])
    grid = pymaze.Grid(rows, columns)
    pymaze.generate_aldous_broder(grid)
    pymaze.render_boxgfx(grid)
