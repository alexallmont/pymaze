# Python Mazes

Loosely ported from 'Mazes for Programmers' by Jamis Buck

    python3 -m venv .venv
    . .venv/bin/activate
    pip install -r requirements.txt
    python examples/main.py 5 5
